#include <stddef.h>
#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

//
static bool            block_is_big_enough( size_t query, struct block_header* block ) {
  return block->capacity.bytes >= query; 
}
// how many pages need for data, that take mem
static size_t          pages_count   ( size_t mem ){
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
// how many mem take all pages that need for mem 
static size_t          round_pages   ( size_t mem ){
  return getpagesize() * pages_count( mem ) ; 
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}
//how many region take memory with query of need
static size_t region_actual_size( size_t query_size ) {
  return size_max( round_pages( query_size), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );



void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query_capacity ) {
  block_capacity query = {.bytes = query_capacity};
  size_t region_size = region_actual_size(size_from_capacity(query).bytes);
  void* region_addres = map_pages(addr, region_size, MAP_FIXED_NOREPLACE );
  
  if(region_addres == MAP_FAILED){
    region_addres = map_pages(addr, region_size, 0);
    if(region_addres == MAP_FAILED){
      return REGION_INVALID;
    }
  }
  block_size first_block_sz = {.bytes = region_size};
  block_init(region_addres, first_block_sz, NULL);
  
  bool extends = (region_addres == addr);

  struct region reg = {.addr = region_addres, .size = region_size, .extends = extends };

  return reg;
}

static void* block_after( struct block_header const* block );
static bool blocks_continuous(struct block_header const* fst, struct block_header const* snd);

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}


/*  освободить всю память, выделенную под кучу */
void heap_term( void ) {

  struct block_header* start = HEAP_START;
  struct block_header* now = start;
  //struct block_header* current = start;
  size_t size_block = 0;
  struct block_header* next = NULL;
  
  while(start != NULL){
    size_block += size_from_capacity(now->capacity).bytes;
    if(now->next != NULL){
    struct block_header* next = now->next;
      if(blocks_continuous(now, next)){
        now = next;
        continue;
      }
    }
    //}
    now->is_free = true;
    next = now->next;
    now = next;
    munmap(start, round_pages(size_block));
    //munmap(start, size_from_capacity(start->capacity).bytes);
    start = now;
    size_block = 0;
  }

  
 // size_t all_size = 0;
  
  //if(start->next == NULL){
   // all_size = size_from_capacity(start->capacity).bytes;
    //munmap(HEAP_START, all_size);
    //return;
  //}

  //while(start->next != NULL){
    //all_size += size_from_capacity(start->capacity).bytes;

    //start->is_free = true;

    //start = start->next;
  //}
  
  //all_size += size_from_capacity(start->capacity).bytes;

  //start->is_free = true;

  //munmap(HEAP_START, round_pages(all_size));

  
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && ((query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY) <= block->capacity.bytes);
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(block_splittable(block, query)){
    //block_size remaining_memory = {block->capacity.bytes - query};
    //block->capacity.bytes = query;

    //block_init(block_after(block), remaining_memory, block->next);
    //block->next = block_after(block); 



    //void* new_block_addr = block_after(block);

    size_t new_block_size = block->capacity.bytes - query;
  
    block_size new_block_size_struct = {.bytes = new_block_size};
    block->capacity.bytes = query;

    block_init(block_after(block), new_block_size_struct, block->next);
    block->next = block_after(block);
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}

//check if blocks located nearly
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd
  ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(
                      struct block_header const* restrict fst,
                      struct block_header const* restrict snd
                      ) {
  return fst->is_free &&
          snd->is_free && 
          blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next_block = block->next;
  if(next_block == NULL) return false;
  if(mergeable(block, next_block)){

    size_t new_capacity = block->capacity.bytes + size_from_capacity(next_block->capacity).bytes;
  
    //block_capacity new_double_block_capacity = {.bytes = new_capacity };

    block->capacity.bytes = new_capacity;
    block->next = next_block->next;
    return true;
    //block_init(block, new_double_block_size, next_block->next);
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
  } type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ){
  struct block_search_result search_result = {.block = block};
  if(block == NULL){
    search_result.block = NULL;
    search_result.type = BSR_CORRUPTED;
    return search_result;
  }
  if (block->next == NULL){
    search_result.block = block;
    bool is_good_block = block_is_big_enough(sz, block);
    if(block->is_free && is_good_block){
      search_result.block = block;
      search_result.type = BSR_FOUND_GOOD_BLOCK;
      return search_result;
    }else{
      search_result.block = block;
      search_result.type = BSR_REACHED_END_NOT_FOUND;
      return search_result;
    }
  }
  while(block->next != NULL){
    while(try_merge_with_next(block));
    bool is_good_block = block_is_big_enough(sz, block);
    if(block->is_free && is_good_block){
      search_result.block = block;
      search_result.type = BSR_FOUND_GOOD_BLOCK;
      return search_result;
    }
      block = block->next;
  }

  search_result.block = block;
  search_result.type = BSR_REACHED_END_NOT_FOUND;
  return search_result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

  struct block_search_result search_result = find_good_or_last(block, query);
  
  if(search_result.type == BSR_CORRUPTED || search_result.type == BSR_REACHED_END_NOT_FOUND){
    return search_result;
  }else{
    split_if_too_big(search_result.block, query);
    search_result.block->is_free = false;
    return  search_result;
  }
}


//add mem to heap, and return block
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {

  //if(block_is_big_enough(query, last)) return last;

  struct region additional_heap = alloc_region(block_after(last), query);
  bool is_invalid = region_is_invalid(&additional_heap);

  if(is_invalid) return NULL;
  
  last->next = additional_heap.addr;
  if(additional_heap.extends){
    bool try = try_merge_with_next(last);
    if(try && block_is_big_enough(query, last)){
      return last;
    }
  }

  return additional_heap.addr;
}


static struct block_search_result try_memalloc_existing_with_grow_heap(struct block_header* restrict last, size_t query){

  struct block_search_result search_result = {};
  search_result = try_memalloc_existing(query, last);
  //if(block_is_big_enough(query, last)) {
  //  search_result.block = last;
  //  search_result.type = BSR_FOUND_GOOD_BLOCK;
  //  return search_result;
  //}
  struct block_header* new_first_block = grow_heap(search_result.block, query);
  if (new_first_block == NULL){
    search_result.block = NULL;
    search_result.type = BSR_CORRUPTED;
    return search_result;
  }
  search_result = try_memalloc_existing(query, new_first_block);
  return search_result;
}




/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  size_t query_size = size_max(query, BLOCK_MIN_CAPACITY);
  //size_t query_size = query;
  struct block_search_result result = try_memalloc_existing(query_size, heap_start);
  
  if(result.type == BSR_CORRUPTED) return NULL;
  if(result.type == BSR_REACHED_END_NOT_FOUND){
    //grow_heap(result.block, query_size);
    //result = try_memalloc_existing(query_size, heap_start);
    //struct block_header* last = result.block;
    result = try_memalloc_existing_with_grow_heap(heap_start, query_size);
    //struct block_header* new_block = grow_heap(result.block, query_size);
    //result = try_memalloc_existing(query_size, new_block);
  }
  return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
