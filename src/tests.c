#include "mem_internals.h"
#include "mem.h"
#include <assert.h>
#include <stdio.h>


static void ussual_success_memalloc(){
  heap_init(4096);
  //struct block_header* start = heap_init(4096);
  struct blcock_header* block = _malloc(256);
  assert(block);
  _malloc(256);
  _malloc(256);

  _free(block);
  //assert(block->is_free);
  debug_heap(stderr, HEAP_START);
  heap_term();
}

static void free_one_from_many(){
  heap_init(4096);
  struct blcock_header* first_block = _malloc(256);
  assert(first_block);
  struct blcock_header* second_block = _malloc(256);
  assert(second_block);
  struct blcock_header* third_block = _malloc(256);
  assert(third_block);
  struct blcock_header* forth_block = _malloc(256);
  assert(forth_block);

  _free(first_block);
  //assert(first_block->is_free);
  //assert(!second_block->is_free);
  //assert(!third_block->is_free);
  //assert(!forth_block->is_free);
  debug_heap(stderr, HEAP_START);


  _free(second_block);
  _free(third_block);
  _free(forth_block);
  heap_term();
}

static void free_two_from_many(){
  heap_init(4096);
  //struct block_header* start = heap_init(4096);
  struct blcock_header* first_block = _malloc(256);
  assert(first_block);
  struct blcock_header* second_block = _malloc(256);
  assert(second_block);
  struct blcock_header* third_block = _malloc(256);
  assert(third_block);
  struct blcock_header* forth_block = _malloc(256);
  assert(forth_block);

  _free(first_block);
  //assert(first_block->is_free);
  _free(third_block);
  //assert(third_block->is_free);

  //assert(!second_block->is_free);
  //assert(!forth_block->is_free);

  debug_heap(stderr, HEAP_START);


  _free(second_block);
  _free(forth_block);
  heap_term();
}

static void memory_end_new_region_extend_old(){
  struct region* heap = heap_init(4096);
  struct block_header* big_block = _malloc(4095);
  assert(big_block);
  struct block_header* small_block = _malloc(128);
  assert(small_block);

  size_t heap_size = heap->size;
  assert(heap_size > 4096);

  debug_heap(stderr, HEAP_START);
  _free(big_block);
  _free(small_block);
  heap_term();
}

static void memory_end_new_region(){
  struct region* heap = heap_init(2048);
  void* new_fix_region = map_pages(HEAP_START + 2049, 1024, MAP_FIXED);

  struct block_header* block = _malloc(128);
  assert(block != new_fix_region);

  munmap(HEAP_START + 2049, 1024);
  _free(block);
  debug_heap(stderr, heap);
  heap_term();
}

void run_tests( void ){
  ussual_success_memalloc();
  free_one_from_many();
  free_two_from_many();
  memory_end_new_region_extend_old();
  memory_end_new_region();
}
